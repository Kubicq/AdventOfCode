summary = impossibles = 0


myFile = open('input.txt' ,'r')
task = myFile.readlines()

for triangel in task:
	sides = triangel.split()
	side = list(map(int, sides))
	summary += 1
	if sum(side) - max(side) <= (sum(side) / 2):
		impossibles += 1

print(summary - impossibles)
myFile.close()