helper = summary = impossibles = 0
newOrder = ''


myFile = open('input.txt' ,'r')
task = myFile.readlines()

'''
A protože neumíme tohle:
for one, two, three in zip(*[iter(triangle_list_part_two)] * 3):
        triangle_list = [one, two, three]
jedeme dám mírně hloupě:
'''

for treeNums in task:
	newOrder += str(list(map(int,treeNums.split()))[0])
	helper += 1
	if helper > 2:
		helper = 0
		newOrder += ';'
	else:
		newOrder += ' '

for treeNums in task:
	newOrder += str(list(map(int,treeNums.split()))[1])
	helper += 1
	if helper > 2:
		helper = 0
		newOrder += ';'
	else:
		newOrder += ' '

for treeNums in task:
	newOrder += str(list(map(int,treeNums.split()))[2])
	helper += 1
	if helper > 2:
		helper = 0
		newOrder += ';'
	else:
		newOrder += ' '

newOrder = newOrder[:-1] #poslední středník pryč!

newTask = newOrder.split(';')
for triangel in newTask:
	sides = triangel.split()
	side = list(map(int, sides))
	summary += 1
	if sum(side) - max(side) <= (sum(side) / 2):
		impossibles += 1

print(summary - impossibles)

myFile.close()