coordX = 2
coordY = 2
code = ''

def showButton(button):
	if button == (1, 3): return '1'
	if button == (2, 3): return '2'
	if button == (3, 3): return '3'
	if button == (1, 2): return '4'
	if button == (2, 2): return '5'
	if button == (3, 2): return '6'
	if button == (1, 1): return '7'
	if button == (2, 1): return '8'
	if button == (3, 1): return '9'

myFile = open('input.txt' ,'r')
instructionsLines = myFile.readlines()
for instructionsLine in instructionsLines:
	for instruction in instructionsLine:
		if instruction == 'R' and coordX < 3: coordX += 1
		if instruction == 'L' and coordX > 1: coordX -= 1
		if instruction == 'U' and coordY < 3: coordY += 1
		if instruction == 'D' and coordY > 1: coordY -= 1
				
	button = coordX, coordY
	code += showButton(button)
print(code)

myFile.close()