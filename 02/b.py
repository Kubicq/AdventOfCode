coordX = 1
coordY = 3
code = ''

def showButton(button):
	if button == (3, 1): return 'D'
	if button == (2, 2): return 'A'
	if button == (3, 2): return 'B'
	if button == (4, 2): return 'C'
	if button == (1, 3): return '5'
	if button == (2, 3): return '6'
	if button == (3, 3): return '7'
	if button == (4, 3): return '8'
	if button == (5, 3): return '9'
	if button == (2, 4): return '2'
	if button == (3, 4): return '3'
	if button == (4, 4): return '4'
	if button == (3, 5): return '1'

allowedCoords = [
	(3, 1),
	(2, 2),
	(3, 2),
	(4, 2),
	(1, 3),
	(2, 3),
	(3, 3),
	(4, 3),
	(5, 3),
	(2, 4),
	(3, 4),
	(4, 4),
	(3, 5)
]

myFile = open('input.txt' ,'r')
instructionsLines = myFile.readlines()
for instructionsLine in instructionsLines:
	for instruction in instructionsLine:
		if instruction == 'R' and (coordX + 1, coordY) in allowedCoords: coordX += 1
		if instruction == 'L' and (coordX - 1, coordY) in allowedCoords: coordX -= 1
		if instruction == 'U' and (coordX, coordY + 1) in allowedCoords: coordY += 1
		if instruction == 'D' and (coordX, coordY - 1) in allowedCoords: coordY -= 1
				
	button = coordX, coordY
	code += showButton(button)
print(code)

myFile.close()