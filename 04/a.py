allowedLetters = 'abcdefghijklmnopqrstuvwxyz'
myFile = open('input.txt' ,'r')
task = myFile.readlines()
sectorIdSum = 0

for roomLine in task:
	letterFrequency = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	finalCode = ''
	leftLetters = ''
	roomName = roomLine[:-12].replace("-", "")
	roomChcksm = roomLine[-7:-2]
	sectorId = int(roomLine[-11:-8])
	
	for letter in roomName:
		letterFrequency[allowedLetters.index(letter)] = roomName.count(letter)

	for num in letterFrequency: #vytvoření kódu dle četnosti
		if max(letterFrequency) > 1:
			finalCode += allowedLetters[letterFrequency.index(max(letterFrequency))]
			letterFrequency[letterFrequency.index(max(letterFrequency))] = 0

	for num in letterFrequency: #vytvoření kódy dle abecedy
		testing = allowedLetters[letterFrequency.index(max(letterFrequency))]
		if max(letterFrequency) > 0:
			leftLetters += allowedLetters[letterFrequency.index(max(letterFrequency))]
			letterFrequency[letterFrequency.index(max(letterFrequency))] = 0
	finalCode += ''.join(leftLetters)
	
	if roomChcksm == finalCode[:5]: sectorIdSum += sectorId

print(sectorIdSum)

myFile.close()