# Hints:
# chr(num)  ord('letter')
# 97 - 122 is a - z

myFile = open('input.txt' ,'r')
task = myFile.readlines()

for roomLine in task:
	roomName = roomLine[:-12].replace("-", " ")
	sectorId = int(roomLine[-11:-8])
	i = 0
	newName = ''

	for letter in roomName:
		if letter != ' ':
			move = sectorId%26
			newOrd = ord(letter) + move
			if newOrd > 122: newOrd -= 26
		else:
			newOrd = ord(letter)

		newName += chr(newOrd) 

	if newName=='northpole object storage': print(sectorId)

myFile.close()