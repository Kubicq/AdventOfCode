ok = 0

myFile = open('input.txt' ,'r')
task = myFile.readlines()

def check_abba(text):
	i = 0
	while i < (len(text)-3):
		if text[i] is text[i+3] and text[i+1] is text[i+2] and text[i] is not text[i+1]:
			return True
	return False
	i += 1

for ip in task:
	outside = []
	inside = []
	z = 0

	#print(ip)
	splitIp = ip.replace('[','-').replace(']','-').split('-')
	
	for oneWord in splitIp:
		if z == 0 or z%2 == 0:
			outside.append(oneWord.strip())
		else:
			inside.append(oneWord)
		z += 1

	#if sum([check_abba(text) for text in outside]) > 0 and not sum([check_abba(text) for text in inside]) > 0:
	for text in outside:
		print(text)
		if check_abba(text):
			ok += 1

print(ok)
myFile.close()