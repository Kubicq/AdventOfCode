import hashlib

i = 0
password = ''
puzzleInput = 'reyedfim'

while len(password) < 8:
	testInput = puzzleInput + str(i)
	hashTest = hashlib.md5(testInput.encode())

	if hashTest.hexdigest()[:5] == '00000':
		password += hashTest.hexdigest()[5]
	i += 1

print(password)