import hashlib, os

i = 0
occupied = ''
password = '________'
puzzleInput = 'reyedfim'
print(password)

while '_' in password:
	testInput = puzzleInput + str(i)
	hashTest = hashlib.md5(testInput.encode())
	if hashTest.hexdigest()[:5] == '00000':
		
		if hashTest.hexdigest()[5].isdigit():
			pos = int(hashTest.hexdigest()[5])
			if str(pos) in occupied:
				pass
			else:
				if pos < 8:
					occupied += str(pos)
					newPassword = password[:pos] + hashTest.hexdigest()[6] + password[pos+1:]

					password = newPassword
					os.system('cls')
					print(password)
	i += 1

os.system('cls')
print(password)