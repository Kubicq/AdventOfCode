from collections import Counter

finalWord = []

myFile = open('input.txt' ,'r')
task = myFile.readlines()

for fivePos in zip(*task):
	pos = Counter(fivePos).most_common()
	finalWord.append(pos[0][0])

print(''.join(finalWord))

myFile.close()