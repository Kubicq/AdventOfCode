distanceX = 0
distanceY = 0
direction = 'T' #T for top, D for down, L for left, R for right
oldDirection = 'T'
position = ['0,0']
uniques = []

myFile = open('input.txt' ,'r')
instructions = myFile.readline().split(', ')

for oneInstr in instructions:
	if 'R' in oneInstr:
		if oldDirection == 'L':
			direction = 'T'
			distanceY += int(oneInstr[1:])
		if oldDirection == 'T':
			direction = 'R'
			distanceX += int(oneInstr[1:])
		if oldDirection == 'R':
			direction = 'D'
			distanceY -= int(oneInstr[1:])
		if oldDirection == 'D':
			direction = 'L'
			distanceX -= int(oneInstr[1:])
		
	if 'L' in oneInstr:
		if oldDirection == 'L':
			direction = 'D'
			distanceY -= int(oneInstr[1:])
		if oldDirection == 'T':
			direction = 'L'
			distanceX -= int(oneInstr[1:])
		if oldDirection == 'R':
			direction = 'T'
			distanceY += int(oneInstr[1:])
		if oldDirection == 'D':
			direction = 'R'
			distanceX += int(oneInstr[1:])	
	
	oldDirection = direction
	
	position.append(str(distanceX) + ',' + str(distanceY))

print (abs(distanceX) + abs(distanceY))

myFile.close()